##Requirements:##

You'll need [Yarn](https://yarnpkg.com/lang/en/docs/install/#mac-tab/) or [npm](https://docs.npmjs.com/cli/install) to install this project's dependencies.

## Run the project:

Run either `yarn install` or `npm install` to install the dependencies.

Run `npm start` to run the local server, which can be found at http://localhost:3000