import React from 'react';
import PlanPage from './plan_page.js';
import getRecommendedPlan from './plans.js';

const logoSrc = require ('../images/sprout-social-logo-icon.svg'
);
class Main extends React.Component {
  constructor() {
    super();
    this.state = {
      recommendedPlan: null
    };
  }

  handleSubmit(e) {
    e.preventDefault();
    const plan = getRecommendedPlan();
    this.setState({ recommendedPlan: plan });
  }

  render() {
    if (this.state.recommendedPlan) {
      return <PlanPage plan={this.state.recommendedPlan} />;
    }

    return (
      <div className="gradient">
        <section className="container">

          <header>
            <img className="logo" src={logoSrc} alt="Sprout Social Logo" />
            <h1 className="header header--light">Find the Sprout Social plan that's right for you.</h1>
          </header>

          <div className="grid grid--hcenter">
            <form className="form" id="form" onSubmit={this.handleSubmit.bind(this)}>

              <div>
                <div className="circle">
                  <div className="circle__content">
                  1
                  </div>
                </div>

                <label className="sub-header" htmlFor="yourProfiles">How many social profiles do you manage? </label>
                <input type="number" name="socialprofiles" id="yourProfiles" required />
              </div>

              <div>
                <div className="circle">
                  <div className="circle__content">
                  2
                  </div>
                </div>

                <label className="sub-header" htmlFor="yourFollowers">How many followers do your social profiles have? </label>
                <input type="number" name="audience" id="yourFollowers" required/>
              </div>

              <div>
                <div className="circle">
                  <div className="circle__content">
                  3
                  </div>
                </div>

                <label className="sub-header" htmlFor="yourTeamsize">How many people are on your social media team? </label>
                <input type="number" name="users" id="yourTeamsize" required />
              </div>

              <input className="button button__primary button--large" type="submit" value="Find my plan!" />

            </form>
          </div>

          <div id="recommendation"></div>
        </section>
      </div>
    );
  }
}

export default Main;