import React from 'react';

class PlanPage extends React.Component {
  renderDetails() {
    const details = this.props.plan.details;

    return (
      <table id="planTable"className="table table--striped" summary="Your recommended subscription plan details.">
        <tbody>
          {Object.keys(details).map(function(key) {
            return (
              <tr className="table__row" key={key}>
                <th className="table__label">{key}</th>
                <td>{details[key]}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  render() {
  	return (
      <div>
        <section className="section">
          <h1 className="header header--light">The plan that is right for you is..</h1>
          <div className="header__highlight">
      		  <h2 className="header__hightlight-title">{this.props.plan.name}</h2>
            <p>{this.props.plan.headline}</p>
          </div>
        </section>

        <section className="container">
          {this.renderDetails()}
        </section>

        <section className="section--padding">
          <div className="container">
            <h2>All Plans include a Free 30-Day Trial</h2>
            <p className="subtitle">Best-in-class social media management and engagement software. Change plans or cancel at any time.</p>
            <a className="button button__primary" href={this.props.plan.url}>Start my free trial</a>
          </div>
        </section>

        <section className="section--light">
          <div className="container">
            <h2 className="header">Interested in checking out other plans?</h2>
            <a className="button button__secondary" href="https://sproutsocial.com/pricing">Compare plans</a>
          </div>
        </section>
    	</div>
    );
  }
}

export default PlanPage;
